# Dark Matter and Dark Energy Cosmological Simulations



`darksim` is a general purpose simulator to calculate effects of dark matter and dark energy,
as well as generate time evolved datasets and 3D animations for model universes.


## Features
-  Calculate the effect of dark matter and dark energy on astrophysical objects
-  Generate time evolved datasets of astrophysical objects
-  Generate relevant equations and numerically solve (Friedmann, Fluid, Acceleration)
-  Generate 3D animations of model universe.

### Maybe Features?
-  Find required dark matter or dark energy parameters based on given acceleration of model universe.
-  Based on parameters of model universe, what death is it headed to?



## Structure
```
darksim (or whatever parent directory)
│   README.md
│   setup.cfg   
│   setup.py
|   requirements.txt
|
└─── examples
│   │   benchmark.py
│   │   empty.py
│   
└───darksim
    │   __init__.py
    │   framework.py
    |   math.py
    |   utils.py
    |   fancy.py
```


`framework.py` is the main class called and holds the model universe's parameters/dataset
as well as methods that apply `math.py` methods. It also handles parallel processing, resource
management, and all public facing API calls.


`math.py` contains all math functions. Some of these will include `integrate(...)`,
`n_body_gravitional_force(...)`, `dark_matter_distribution_to_point(...)`, `vect_transpose(...)`, etc.


`utils.py` holds general purpose utilities that don't quite fit into `framework.py` or `math.py`.
If we end up using a database like PostgreSQL, database interaction methods will be here.


`fancy.py` contains extra methods to make things nice like progress bars, etc.


## Usage
wait until we get into it to see what changes. usually a lot changes, like the entire project being
refactored, multiple times over...


## Datastructures
This section is meant to give shapes, types, and what's what for all data-structures. A preliminary
example is below for ```initial_data```.

#### Initial Data:
Initial data for `N` bodies with position `(x, y, z)`, velocity (apparent/absolute) `(vx, vy, vz)`, mass (distribution, point). Later down the road when we are computing our final simulations if the number of bodies gets too large we can convert `np.array` to `dask.array` to not load the entire
array into memory. This has some drawbacks, like not having `np.linalg` functionality.

```py
initial_data ->  # Holds all parameters of N-bodies
  type = dict
  keys = [
    'position', |-> type = np.ndarray
    'velocity', |-> dtypes = np.float32
    'mass',     |-> shape = (N, 3)

    # Origin of coordinate system
    'position_ref', -> type = tuple(),
                    |  dtypes = np.float32,
                    |  shape = (3,)
    # velocity type relative to position data
    'velocity_type' -> type = str(),
                    |  values = 'apparent' or 'absolute'
  ]

>>> initial_data
{
  'position': array([
    [0.00349363, 2.69207237, 1.16708297],
    ...,
    [0.41483391, 0.67744255, 2.46923287]
  ]),
  'velocity': array([
    [-225.98517234,  333.0185668 ,  286.35241416],
    ...,
    [ 250.33061085, -316.96274877, -41.61383232]
  ]),
  'position_ref': (0., 0., 0.),
  'velocity_type': 'absolute'
}
```
