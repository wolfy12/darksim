import numpy as np
import pandas as pd

import dask.distributed as dask_dist
import dask.dataframe as dd
import dask.array as da

# import darksim.dmath as dmath
# import darksim.utils as utils
# import darksim.fancy as fancy

# Temp imports
import darksim.dmath as dmath
import darksim.utils as utils
import darksim.fancy as fancy


class ModelUniverse:
    """ Create a model universe from density parameters. This universe can be time evolved

    :args:
    data_file_name (str): Name of .csv file with initial data. See utils.DataHandler class for details on file use.

    :kwargs: All kwargs are density parameters to be used when constructing the Friedmann Equation
    vis_matter (float, 0 < matter < 1): Matter density parameter for visible matter only
    dark_matter (float, 0 < dark_matter < matter): Dark matter density parameter for dark matter only
    radiation (float, 0 < radiation < 1): Radiation density parameter
    lambda (float, 0 < lambda < 1): Cosmological constant parameter

    """
    ingredients = ['vis_matter', 'dark_matter', 'radiation', 'lambda']
    def __init__(self, **kwargs):
        self.density_params = kwargs

        # Verify density params
        for key,value in self.density_params.items():
            if value < 0 or value > 1:
                raise ValueError(f"Density parameter {key}={value} is not in valid range of 0 < {key} < 1")

        # Get total density (this accounts for curvature in FE)
        self.total_density = sum(self.density_params.values())

        self.friedmann = self._construct_friedmann()

        # Solve friedmann equation for scale factor a
        # self.scale_factor_func = dmath.solve(self.friedmann, a)


    def _construct_friedmann(self):
        """ Constructs callable function of right side of friedmann equation """
        # For ingredients not given, set equal to 0
        for ing in self.ingredients:
            if ing not in self.density_params.keys():
                self.density_params[ing] = 0.0

        return lambda a: \
            self.density_params['radiation'] / np.power(a, 4) +\
            (self.density_params['vis_matter'] + self.density_params['dark_matter']) / np.power(a, 4) +\
            self.density_params['lambda'] +\
            (1 - self.total_density) / np.power(a, 2)


    @staticmethod
    def _write_time_step(data):
        pass


    @staticmethod
    def _load_inital_data(self, file_name):
        pass


    def time_step(self, data, step_size):
        pass


    def time_evolve(self, data, start, end, step_size):
        pass



    def start(self, start_data_file_name):
        # Load initial data
        pd.read_csv(start_data_file_name)
